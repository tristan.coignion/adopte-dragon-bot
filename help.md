Voici l'**aide** du bot *Adopte un Dragon*

Pour jouer au jeu, tapez `$run`

Pour quitter le jeu, tapez `$quit`

Pour voir les joueurs présents, tapez `$players`

Pour voir la liste des thèmes présents, tapez `$themes`

Le reste sera expliqué au fur et à mesure du jeu, si vous avez un autre soucis,
contactez The Doot#1358