import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="adopte-dragon-bot", # Replace with your own username
    version="0.2.3",
    author="Tristan Coignion",
    author_email="tristan.coignion@gmail.com",
    description="A package that provides a bot to use in discord",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)