from context import adopteDragonBot
from adopteDragonBot.die import Die

def setup():
  die = Die("Red")
  die.load_faces_from_file("./resources/dice/colored/red.dat")
  return die

def test_load_correct():
  d = setup()
  assert(d.faces[0] == "à mon avis")
  assert(d.faces[5] == "Quand soudain")

def test_roll_returns_str():
  d = setup()
  assert(type(d.roll() == type("")))