SAME_DIGITS_MESSAGE = "Le thème de votre choix ! (Choix libre)"
class ThemeCard:
  """
  A ThemeCard is an object that stores a list of themes indexed
  by two digits. It also has an unique identifier

  Attributes
  ----------
  ident : str
    the identificator of the card
  themes : [[]]
    a list, containing multiple lists of themes

  Methods
  -------
  load_themes_from_file(filepath)
    loads all the themes from a file
  get_themes(digit1, digit2)
    returns the two possible themes for these two digits
  get_all_themes()
    returns all the

  Static Methods
  --------------
  convert_one_theme_to_str(theme, i, j):
    returns a string of the theme and its associated digit
  remove_first_element_of_first_list(l)
    ?
  """

  def __init__(self, ident):
    """ 
    Parameters :
    ident : str
      the identificator of the card, usually a single letter
    """
    self.ident = ident
    self.themes = [[]]

  @staticmethod
  def _process_raw_list_of_themes(listOfThemes):
    """Takes a `raw` list of themes and returns one list containing lists containing themes
    ! NOT IMPLEMENTED
    """
    pass

  def load_themes_from_file(self, filepath):
    """Reads a file containing all the themes of the card and stores the themes in self.themes
    
    Each line represent a theme, and a blank line represents a new subcategory
    """
    lines = []
    with open(filepath, encoding='utf8') as file:
      lines = file.readlines()
    self._process_lines(
      list(map(lambda line: line.strip("\n"), lines)),
      0,0)
  
  def _process_lines(self, lines, i, j):
    """
    Process a the lines read from a file
    
    if the first line is a theme, stores it and call itself for the next line
    """
    if(i == j):
      # Ex : 66 ; 44 ; 33
      self.themes[i].append(SAME_DIGITS_MESSAGE)
      self._process_lines(lines, i, j+1)
    else:
      if lines == []:
        return
      else:
        if lines[0] == "":
          # Blank line
          self.themes.append([]) # Add a new list to the list of list of themes
          self._process_lines(lines[1:], i+1, 0)
        else:
          # Line with a theme
          self.themes[i].append(lines[0]) # Add a new theme
          self._process_lines(lines[1:], i, j+1)

  def get_themes(self, digit1, digit2):
    """
    Return the themes associated to these two digits

    If the theme is 12 : digit1 == 1 and digit2 == 2

    it's associated to the themes [0][1] and [1][0]
    
    Throws index error
    """
    return (self.themes[digit1-1][digit2-1], self.themes[digit2-1][digit1-1])

  def __eq__(self, other):
    """Two ThemeCards are equal iff their id are equals"""
    return isinstance(other, ThemeCard) and self.ident == other.ident

  def __str__(self):
    res = "Theme : " + self.ident + "\n"
    res = ThemeCard.convert_themes_to_str(self.themes, res, 1, 1)
    return res

  @staticmethod
  def convert_one_theme_to_str(theme, i, j):
    return "{0}{1} : {2}\n".format(i,j,theme)

  @staticmethod
  def remove_first_element_of_first_list(l):
    """
    Removes the first element of the first list 

    ? Don't know what it's for

    Example :

    >>> removeFirstElementOfFirstList([[1,2,3],[4,5,6],[7,8,9]])
    [[2,3],[4,5,6],[7,8,9]]
    """
    # print(l[0][1:])
    # print(l[1:])
    # print(l[0][1:] + l[1:])
    return [l[0][1:]] + l[1:]

  @staticmethod
  def convert_themes_to_str(themes_list, acc, i, j):
    if themes_list == []:
      # No more themes
      return acc
    if themes_list[0] == []:
      # No more themes for this i
      return ThemeCard.convert_themes_to_str(themes_list[1:], acc, i+1, 1)
    else:
      # accumulate the result, and remove the processed theme
      return ThemeCard.convert_themes_to_str(
        ThemeCard.remove_first_element_of_first_list(themes_list),
        acc + ThemeCard.convert_one_theme_to_str(themes_list[0][0], i, j),
        i,
        j+1)

if __name__ == "__main__":
  card = ThemeCard("A")
  card.load_themes_from_file("themes/A.dat")
  print(card)