from .game_state import GameState
from .player import Player
from .theme_card import ThemeCard
from .color import Color
from .die import Die
from os import listdir
from os.path import isfile, join, basename, splitext
import random
import logging

class AdopteDragonGame:
  """
  Attributes
  ----------
  players :               [Player]
  current_player :        Player
  current_player_index :  int
  theme_cards :           [ThemeCard]
  chosen_card :           ThemeCard
  two_available_themes :  (str, str)
  chosen_theme :          str
  dice :                  [Die]
  current_colored_die_index : int
  white_die :             Die
  is_white_rolled :       bool
  black_die :             Die
  nb_black_rolls_left :   int
  last_two_d6_rolls :     (int, int)
  last_simple_d6_roll :   int
  rounds_played :         int
  state :                 GameState
  """
  def __init__(self):
    self.players = [] # List of Players
    self.current_player = None
    self.current_player_index = 0

    self.theme_cards = [] # List of ThemeCards
    self.chosen_card = None
    self.two_available_themes = (None, None)
    self.chosen_theme = None

    self.dice = []
    self.current_colored_die_index = 0
    self.white_die = None
    self.is_white_rolled = False
    self.black_die = None
    self.nb_black_rolls_left = 3
    
    self.last_two_d6_rolls = (None, None)
    self.last_simple_d6_roll = None

    self.rounds_played = 0
    self.state = GameState.IDLE


  def restart(self):
    self.__init__()

  def restart_round(self):
    """
    Restarts the round by incrementing the number of rounds played and resetting round-related attributes such as :
      - chosen_card
      - two_available_themes
      - chosen_theme
      - current_colored_die_index
      - is_white_rolled
      - nb_black_rolls_left
    """
    self.chosen_card = None
    self.two_available_themes = (None, None)
    self.chosen_theme = None
    self.current_colored_die_index = 0
    self.is_white_rolled = False
    self.nb_black_rolls_left = 3
    self.rounds_played += 1
    logging.info("The round number {} has begun".format(self.rounds_played))

  ###########
  # PLAYERS #
  ###########

  def add_player(self, user):
    """
    Adds a player to the game.

    Parameters
    ----------
    user : A discord user

    Raises
    ------
    ValueError
      If the user is already a player
    """
    if (Player(user) in self.players):
      raise ValueError("The user is already a player !")
    else:
      self.players.append(Player(user))

  def is_user_playing(self, user):
    """
    Returns whether or not a user is already a player
    """
    return any(list(map(lambda player: player.user == user, self.players))) #! TO TEST

  def remove_player(self, user):
    """
    Removes a player from the game

    Raises
    ------
    ValueError
      If the user is not a player
    """
    try:
      self.players.remove(Player(user))
    except ValueError:
      raise ValueError("The user is not a player !")

  def add_score_to_current_player(self, scoreToAdd):
    """
    Adds `scoreToAdd` to the current player
    """
    if (self.current_player == None):
      raise RuntimeError("There is no current player in the game !")
    else:
      self.current_player.score += scoreToAdd
      

  def switch_to_next_player(self):
    """
    Change current_player to the next player in the list (or the first if it loops back)
    """
    if (self.players == []):
      raise RuntimeError("There is no player in the game !")
    if (self.current_player == None):
      raise RuntimeError("There is no current player in the game !")
    self.current_player_index = (self.current_player_index + 1) % len(self.players)
    self.current_player = self.players[self.current_player_index]

  def set_first_player(self):
    """
    Sets the first player in the list of players as the current player
    """  
    try:
      self.current_player = self.players[0]
      self.current_player_index = 0
    except IndexError:
      raise RuntimeError("There a no players in the game !")

  def print_scores(self):
    """
    Prints each player and its score and the best player
    """
    # TODO : Rewrite it to make the print better
    best_player = None
    for player in self.players:
      if player.score > best_player.score:
        best_player = player
      print(player.user.name, player.score)
    print(best_player)
    
  def player_to_string(self):
    """
    Returns a string representation of all the players in a string
    """
    res = "Les joueurs sont : \n"
    for player in self.players:
      res += str(player) + "\n" 
    return res

  ###############
  # THEME CARDS #
  ###############

  def load_theme_cards(self, directory):
    """
    Reads a file to load the game cards in the game
    """
    all_files = [join(directory,f) for f in listdir(directory) if isfile(join(directory, f))]
    self.theme_cards = list(map(AdopteDragonGame._create_theme, all_files))
    
  @staticmethod
  def _create_theme(filepath):
    """
    Creates a theme with a file
    """
    # print(splitext(basename(filepath))[0])
    theme_card = ThemeCard(splitext(basename(filepath))[0])
    theme_card.load_themes_from_file(filepath)
    return theme_card

  def get_all_theme_cards_ident(self):
    """
    Returns a string with all the theme cards identifiers
    """
    return list(map(lambda themeCard : themeCard.ident, self.theme_cards))

  def get_theme_card_by_ident(self, ident):
    """
    Returns the theme card with the corresponding id

    raise ValueError if the theme does not exist
    """
    try:
      return AdopteDragonGame.find(
        (lambda theme_card : theme_card.ident == ident),
        self.theme_cards)
    except ValueError:
      raise ValueError("The searched theme does not exist")

  @staticmethod
  def find(condition, l):
    """
    Returns the first element in the list that respects
    the condition

    ? Should be tested ?
    """
    if l == []:
      raise ValueError("No element in the list respect this condition")
    else:
      if condition(l[0]):
        return l[0]
      else:
        return AdopteDragonGame.find(condition, l[1:])

  ########
  # DICE #
  ########

  @staticmethod
  def roll_die():
    """Returns a random number between 1 and 6"""
    return random.randint(1,6)

  @staticmethod
  def roll_two_dice():
    """Returns two random numbers between 1 and 6 in a tuple"""
    return (random.randint(1,6), random.randint(1,6))

  def load_dice(self, directory):
    """
    Loads all the dice from a directory

    The directory must have two subdirectory named colored and wb
    """
    # TODO : Handle when no subdirectory
    colored_directory = join(directory, "colored")
    wb_directory = join(directory, "wb")
    all_colored_files = [join(colored_directory,f) for f in listdir(colored_directory) if isfile(join(colored_directory, f))]
    # print(allFiles)
    self.dice = sorted(list(map(AdopteDragonGame.create_die, all_colored_files)))
    self.white_die = AdopteDragonGame.create_die(join(wb_directory, "white.dat"))
    self.black_die = AdopteDragonGame.create_die(join(wb_directory, "black.dat"))

  @staticmethod
  def create_die(filepath):
    """
    Create a die from a file
    """
    # TODO : Handle error when color not defined
    color = Color[splitext(basename(filepath))[0].upper()]
    die = Die(color)
    die.load_faces_from_file(filepath)
    # print(die.color)
    return die

  def reset_dice(self):
    """
    Resets the dice

    The colored die counter is reset
    The whites are reset
    the blacks are reset
    """
    self.current_colored_die_index = 0
    self.is_white_rolled = False
    self.nb_black_rolls_left = 3

  def get_current_colored_die(self):
    """
    Returns the colored die that is next to roll
    """
    if(self.current_colored_die_index >= len(self.dice)):
      raise RuntimeError("There are no more dice available !")
    return self.dice[self.current_colored_die_index]
    
  def roll_next_colored_die(self):
    """
    Rolls the next colored die on the list and returns the result (str)
    """
    if(self.current_colored_die_index >= len(self.dice)):
      raise RuntimeError("There are no more dice available !")
    res = self.dice[self.current_colored_die_index].roll()
    self.current_colored_die_index += 1
    return res

  def is_any_colored_die_left(self):
    """
    Returns whether or not there are any colored die left to roll
    """
    return self.current_colored_die_index < len(self.dice)

  def roll_white_die(self):
    """
    Rolls the white die and returns the result

    Sets is_white_rolled to True
    """
    if(self.is_white_rolled):
      raise RuntimeError("The white die has already been rolled !")
    self.is_white_rolled = True
    return self.white_die.roll()

  def roll_black_die(self):
    """
    Rolls the black die and returns the result

    Decrement the number of black rolls left
    """
    if(self.nb_black_rolls_left <= 0):
      raise RuntimeError("There are no more black rolls !")
    self.nb_black_rolls_left -= 1
    return self.black_die.roll()
    

  def is_any_black_die_left(self):
    """
    Returns whether or not there are any roll left of the black die
    """
    return self.nb_black_rolls_left > 0