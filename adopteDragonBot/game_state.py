from enum import Enum
class GameState(Enum):
  IDLE                = 1 
  PREPARING_GAME      = 2
  CHOOSING_THEME_CARD = 3
  CHOOSING_THEME      = 4
  PLAYING_ROUND       = 5
  VOTING              = 6
