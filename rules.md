Comment jouer au **DRAGON** ?

__CHOISISSEZ UN THÈME__
Choisissez une carte de thème.

Le premier joueur lance les deux dés numérotés (d6).
    * Il choisit l'un des deux thèmes suggérés par les dés.
Exemple: s'il fait **1** et **6**, il peut prendre le thème 16 ou 61.
    * S'il fait un **double**, les autres joueurs proposent des thèmes inventés par eux, et il choisit celui qui l'inspire.
    * S'il fait un **double 6**, il doit obligatoirement raconter... *Comment j'ai adopté un dragon !*

__RACONTEZ VOTRE HISTOIRE__
Le joueur/narrateur lance le dé jaune et démarre son récit avec les mots obtenus.
Quand il en a envie ou besoin, il lance un à un les dés suivants et continue à raconter en se servant des mots indiqués.
Durant son histoire, il devra lancer le dé blanc - au moment de son choix - mais impérativement avant de conclure avec le cinquième et dernier dé.

__MESUREZ VOTRE SUCCÈS__
A l'issue de sa prestation le narrateur s'auto-évalue en jetant un dé numéroté et obtient le score indiqué.

Si les autres joueurs estiment que son histoire méritait mieux, ils peuvent l'inviter à lancer l'autre dé numéroté, afin qu'il cumule la valeur des deux dés.
*Ce système peut se révéler cruel, or l'amour est cruel et ce jeu n'est qu'amour!*

Le joueur suivant relance les deux dés numérotés, choisit un thème et raconte son histoire avec les dés-connecteurs. Après un tour complet, la première partie est terminée.

__MAIS MESSIEURS LES AUTEURS... A QUOI SERT LE DÉ NOIR ?__
Bonne question Kévin ! C'est un dé de contre qui permet une variante : pendant l'histoire du narrateur, les autres joueurs peuvennt le lancer pour lui couper l aparole.
Le conteur doit alors répondre et adapter son histoire.
L'usage du dé noir est limité à **3 lancers par histoire**, qu'ils soient effectués par un ou plusieurs des autres jours.
