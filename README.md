# Adopte Dragon Bot

This application is a discord bot that simulates and run the game "Comment j'ai Adopté un Dragon."

## How to execute

Run `python -m adopteDragonBot.adopte_dragon_bot` while in root

## Commands

`$run`
`$help`
`$rules`
`$quit`
`$participate`
`$choose [nameOfTheme]`
`$choose [0|1]`
`$roll [next|white|black]`
`$start`
`$players`
`$themes`