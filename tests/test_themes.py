from context import adopteDragonBot
from adopteDragonBot.theme_card import ThemeCard, SAME_DIGITS_MESSAGE

def setup():
  theme_card = ThemeCard("A")
  theme_card.load_themes_from_file("./resources/themes/A.dat")
  return theme_card

def test_double_digits_is_correct_message():
  theme_card = setup()
  assert(theme_card.themes[0][0] == SAME_DIGITS_MESSAGE)
  assert(theme_card.themes[1][1] == SAME_DIGITS_MESSAGE)

def test_load_themes_correct():
  theme_card = setup()
  assert(theme_card.themes[0][1] == "Theme 12")
  assert(theme_card.themes[1][0] == "Theme 21")
  assert(theme_card.themes[2][3] == "Theme 34")

def test_get_themes_returns_two_themes():
  theme_card = setup()
  assert((theme_card.get_themes(1, 2) == (theme_card.themes[0][1], theme_card.themes[1][0])) or
         (theme_card.get_themes(1, 2) == (theme_card.themes[1][0], theme_card.themes[0][1])))

def test_two_themes_equal_iff_ident_equal():
  t1 = setup()
  t2 = setup()
  t3 = ThemeCard("B")
  assert (t1 == t2)
  assert (t1 != t3)