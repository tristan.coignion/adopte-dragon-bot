from context import adopteDragonBot
import adopteDragonBot.adopte_dragon_bot as adb
import adopteDragonBot.adopte_dragon_game as adg
from adopteDragonBot.game_state import GameState
from unittest.mock import Mock, MagicMock
import pytest
import asyncio

class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)

def run_coroutine(f, *args, **kwargs):
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(f(*args, **kwargs))
    loop.close()
    return result

@pytest.fixture
def game():
    g = adg.AdopteDragonGame()
    g.load_theme_cards("resources/themes")
    g.load_dice("resources/dice")
    return g

@pytest.fixture
def bot(game):
    bot = adb.AdopteDragonBot(game)
    return bot

async def test_run_game_sets_state_to_preparing_game(bot):
    assert(isinstance(bot, adb.AdopteDragonBot))
    channel = MagicMock()
    run_coroutine(bot.run_game(channel))
    assert(bot.game.state == GameState.PREPARING_GAME)

def test_run_game_sets_participation_message_id(bot):
    pass

def test_run_game_adds_two_reactions(bot):
    pass

def test_start_game_only_works_when_enough_players(bot):
    pass

def test_start_game_calls_prepare_round(bot):
    pass

def test_start_game_sets_first_player(bot):
    pass

def test_prepare_round_sets_the_state(bot):
    pass

def test_prepare_round_calls_send_list_of_theme_cards(bot):
    pass

# ? send_list_of_theme_cards

def test_send_specific_theme_sends_correct_theme(bot):
    pass