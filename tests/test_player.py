from context import adopteDragonBot
from adopteDragonBot.player import Player
from unittest.mock import Mock

def setup():
  user = Mock()
  user.id = 42
  p = Player(user)
  return p

def test_starts_score_at_0():
  p = setup()
  assert(p.score == 0)

def test_two_players_equals_iff_user_id_equals():
  p = setup()
  user2 = Mock()
  user2.id = 42
  user3 = Mock()
  user3.id = 43
  p2 = Player(user2)
  p3 = Player(user3)
  assert(p == p2)
  assert(p != p3)
