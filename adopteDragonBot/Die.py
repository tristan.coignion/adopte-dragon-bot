from .color import Color
from functools import total_ordering
import random

@total_ordering
class Die:
  """
  A die is represented by multiple faces and a color from Color.

  Attributes
  ----------
  color : Color
    a color from the Color class
  faces : [str]
    a list of messages that represent the faces

  Methods
  -------
  load_faces_from_file(filepath)
    Loads the die faces from a file
  roll()
    Returns a random face
  """
  
  def __init__(self, color):
    """
    Parameters
    ----------
    color : Color
      an enumerated color from Color
    """
    self.color = color
    self.faces = [] # List of str
  
  def load_faces_from_file(self, filepath):
    """
    Loads the faces of the die from a file containg the faces message

    In the file, each line represents one face

    Parameters
    ----------
    filepath : str
      The path to the file to read
    """
    lines = []
    with open(filepath, encoding='utf8') as file:
      lines = file.readlines()
    self.faces = list(map(lambda line: line.strip("\n"), lines))

  def roll(self):
    """
    Returns a random face of the die.

    Returns
    -------
    str
      a message on a random face of the die
    """
    return random.choice(self.faces)

  def __eq__(self, other):
    """
    Two dice are equal iff they are the same color
    """
    return isinstance(other, Die) and self.color == other.color

  def __gt__(self, other):
    """
    One dice is greater than another iff its color is greater than the other
    """
    return isinstance(other, Die) and self.color > other.color

  def __str__(self):
    """
    "`color` : `faces`"
    """
    return "{0} : {1}".format(self.color, self.faces)