from enum import IntEnum

COLOR_TO_STR_FR = {
  -1 : "Noir",
  0  : "Blanc",
  2  : "Jaune",
  4  : "Orange",
  6  : "Rouge",
  8  : "Violet",
  10 : "Bleu"
}

COLOR_TO_EMOJI = {
  -1 : "⬛",
  0  : "⬜",
  2  : "🟨",
  4  : "🟧",
  6  : "🟥",
  8  : "🟪",
  10 : "🟦"
}

class Color(IntEnum):
  BLACK  = -1
  WHITE  = 0
  YELLOW = 2
  ORANGE = 4
  RED    = 6
  PURPLE = 8
  BLUE   = 10
  

  def get_color_french_name(self):
    return COLOR_TO_STR_FR[self.value]

  def to_emoji(self):
    return COLOR_TO_EMOJI[self.value]