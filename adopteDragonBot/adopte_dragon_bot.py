import discord
from .adopte_dragon_game import AdopteDragonGame
from .game_state import GameState
from .color import Color
import warnings
import logging

# client = discord.Client()

THUMBS_UP_EMOJI = "👍"
THUMBS_DOWN_EMOJI = "👎"
PERFECT_EMOJI = "👌"
CHECK_EMOJI = "✅"
QUESTION_EMOJI = "❔"
# TODO: Add EMOJI_MODE


class AdopteDragonBot(discord.Client):
    """
    AdopteDragonBot is a Discord Bot which simulates a game of Adopte un Dragon (see adopte_dragon_game.py)

    So far, it only works on one channel at a time.
    """

    def __init__(self, game):
        super(AdopteDragonBot, self).__init__()
        self.game = game
        self.participationMessageId = None
        self.theme_card_message_to_ident = {}
        self.rolled_themes = {}  # Message.id : theme
        self.prompt_to_roll_msg_id = None
        self.prompt_to_vote_msg_id = None
        self.users_left_to_vote = []
        self.votes = []

    async def on_ready(self):
        """
        When clientUser is connected
        """
        print('We have logged in as {0.user}'.format(client))

    # Primary routers
    # ---------------

    async def on_message(self, message):
        """
        When a message is sent
        """
        if message.author == self.user:
            return
        logging.debug("Message {0} detected : {1}".format(message.id, message.content))
        if message.content.startswith('$hello'):
            await message.channel.send('Hello!')
            await message.author.send("hello there")

        elif message.content.startswith('$run'):
            await self.handle_run(message)

        elif message.content.startswith("$start"):
            await self.handle_start(message)

        elif message.content.startswith("$players"):
            await self.handle_players(message)

        elif message.content.startswith("$themes"):
            await self.handle_themes(message)

        elif message.content.startswith("$choose"):
            await self.handle_choose(message)

        elif message.content.startswith("$roll"):
            await self.handle_roll(message)

        elif message.content.startswith("$help"):
            await self.display_help(message.channel)

        elif message.content.startswith("$quit"):
            await self.quit_game(message.channel)

        elif message.content.startswith("$rules"):
            await self.display_rules(message.channel)

        else:
            logging.info("Message not recognized")

    async def on_reaction_add(self, reaction, user):
        """
        When a reaction is added to a message
        """
        if user == self.user:
            return
        logging.debug("Reaction detected on {0}: {1}".format(reaction.message.id, reaction.emoji))
        if self.game.state == GameState.PREPARING_GAME:
            # Check the message reacted is the correct message
            if reaction.message.id == self.participationMessageId:
                if reaction.emoji == THUMBS_UP_EMOJI:
                    await self.add_player(reaction.message.channel, user)
                if reaction.emoji == PERFECT_EMOJI:
                    await self.start_game(reaction.message.channel)
        elif self.game.state == GameState.CHOOSING_THEME_CARD:
            if user == self.game.current_player.user:
                # If the reaction is to a message containing a theme card
                if reaction.message.id in self.theme_card_message_to_ident:
                    ident = self.theme_card_message_to_ident[reaction.message.id]
                    if reaction.emoji == CHECK_EMOJI:
                        # Choose this theme card
                        await self.choose_theme_card(reaction.message.channel, ident)
                    if reaction.emoji == QUESTION_EMOJI:
                        # More info about theme card
                        await self.send_specific_theme(reaction.message.channel, ident, with_emoji=True)
        elif self.game.state == GameState.CHOOSING_THEME:
            if user == self.game.current_player.user:
                # If the reaction is to a message containing a theme
                if reaction.message.id in self.rolled_themes:
                    if reaction.emoji == CHECK_EMOJI:
                        # Choose this theme
                        await self.choose_theme(reaction.message.channel, self.rolled_themes[reaction.message.id])
        elif self.game.state == GameState.PLAYING_ROUND:
            # If the reaction is to the roll prompt
            if reaction.message.id == self.prompt_to_roll_msg_id:
                if user == self.game.current_player.user:
                    # Check if reaction is the next color
                    if reaction.emoji == self.game.dice[self.game.current_colored_die_index].color.to_emoji():
                        await self.roll_colored_die(reaction.message.channel)
                    # Check if reaction is white
                    if reaction.emoji == Color.WHITE.to_emoji():
                        await self.roll_white_die(reaction.message.channel)
                else: # Another player (not the current player)
                    # Check if reaction is black
                    if reaction.emoji == Color.BLACK.to_emoji():
                        await self.roll_black_die(reaction.message.channel)
        elif self.game.state == GameState.VOTING:
            if reaction.message.id == self.prompt_to_vote_msg_id:
                if user in self.users_left_to_vote:
                    # Check if reaction is a thumb up or down
                    if reaction.emoji == THUMBS_UP_EMOJI:
                        await self.vote_up(reaction.message.channel, user)
                    elif reaction.emoji == THUMBS_DOWN_EMOJI:
                        await self.vote_down(reaction.message.channel, user)


    # Secondary routers
    # -----------------
    async def handle_run(self, message):
        if self.game.state == GameState.IDLE:
            await self.run_game(message.channel)
        else:
            await self.send_message_cannot_use_now(message.channel)

    async def handle_start(self, message):
        if self.game.state == GameState.PREPARING_GAME:
            await self.start_game(message.channel)
        else:
            await self.send_message_cannot_use_now(message.channel)

    async def handle_players(self, message):
        if self.game.state != GameState.IDLE:
            await message.channel.send(self.game.player_to_string())
        else:
            await self.send_message_cannot_use_now(message.channel)

    async def handle_themes(self, message):
        argument = message.content.lstrip("$themes").strip()
        if argument == "":
            await self.send_list_of_theme_cards(message.channel)
        else:
            await self.send_specific_theme(message.channel, argument)

    async def handle_choose(self, message):
        argument = message.content.lstrip("$choose").strip()
        if self.game.state == GameState.CHOOSING_THEME_CARD:
            await self.choose_theme_card(message.channel, argument)
        if self.game.state == GameState.CHOOSING_THEME:
            await self.choose_theme(message.channel, argument)
        else:
            await self.send_message_cannot_use_now(message.channel)

    async def handle_roll(self, message):
        """
        Called with "$roll".

        Possible arguments are : "next", "white" and "black". They decide which die to roll
        """
        argument = message.content.lstrip("$roll").strip()
        if self.game.state == GameState.PLAYING_ROUND:
            # Check message author is current player
            if message.author == self.game.current_player.user:
                if "next" in argument:
                    await self.roll_colored_die(message.channel)
                elif "white" in argument:
                    await self.roll_white_die(message.channel)
            else:
                if "black" in argument:
                    await self.roll_black_die(message.channel)
        else:
            await self.send_message_cannot_use_now(message.channel)

    # Controllers
    # -----------

    async def run_game(self, channel):
        """
        Starts the game's preparation

        Set the state to PREPARING_GAME and sets participationMessageId
        """
        self.game.restart()
        self.game.load_theme_cards("adopteDragonBot/resources/themes")
        self.game.load_dice("adopteDragonBot/resources/dice")
        self.game.state = GameState.PREPARING_GAME
        logging.info("Changed the state to PREPARING_GAME")
        msg_to_send = "Démarrage du jeu...\n"
        msg_to_send += "---------------------\n"
        msg_to_send += "SELECTION DES JOUEURS\n"
        msg_to_send += "Appuyez sur {0} pour participer, et appuyez sur {1} pour commencer la partie".format(THUMBS_UP_EMOJI, PERFECT_EMOJI)
        message = await channel.send(msg_to_send)
        # Stores the message id for future reference
        self.participationMessageId = message.id
        logging.debug("ParticipationMessageId : {}".format(message.id))
        await message.add_reaction(THUMBS_UP_EMOJI)
        await message.add_reaction(PERFECT_EMOJI)

    async def display_help(self, channel):
        """
        Reads and send the contents of the file help.md
        """
        with open("./help.md", encoding="utf-8") as fhelp:
            lines = fhelp.readlines()
        logging.info("Printing help")
        await channel.send("".join(lines))

    async def display_rules(self, channel):
        """
        Reads and displays the contents of the file rules.md
        """
        with open("./ruleS.md", encoding="utf-8") as frules:
            lines = frules.readlines()
        logging.info("Printing rules")
        await channel.send("".join(lines))

    async def quit_game(self, channel):
        """
        Quits the current game, resets it and returns the state to idle
        """
        self.game.state = GameState.IDLE
        logging.info("Arret du jeu")
        await channel.send("Arrêt du jeu... Utilisez $run pour rejouer une partie !")

    async def add_player(self, channel, user):
        """
        Adds a player in the game

        Cannot add twice the same player
        """
        if(self.game.is_user_playing(user)):
            logging.warning("This user tried to participate, but was already playing : {}".format(user.name))
        else:
            logging.info("Adding player : {0}".format(user.name))
            self.game.add_player(user)
            await channel.send("{} will participate !".format(user))

    async def start_game(self, channel):
        """
        Starts the game, iff there are enough players

        sets the first player

        call prepare_round()
        """
        # Check there are enough players
        if len(self.game.players) >= 2:  # TODO : Make constant
            await channel.send("The game has started ! The players are : \n")
            await channel.send(self.game.player_to_string())
            # The first player in the list becomes the current player
            self.game.set_first_player()
            logging.info("The first player is : {}".format(self.game.current_player.user.name))
            await self.prepare_round(channel)
        else:
            # TODO : Precise how many there should be
            logging.warning("Not enough players to start the game")
            await channel.send("Il n'y a pas assez de joueurs pour commencer la partie!")

    async def prepare_round(self, channel):
        """
        Prepares the incoming round

        Sets the state of the game to CHOOSING_THEME_CARD
        """
        self.game.state = GameState.CHOOSING_THEME_CARD 
        logging.info("Changed the state to CHOOSING_THEME_CARD")
        logging.info("Current player : ".format(self.game.current_player.user.name))
        self.game.restart_round() # Increments turn number
        msg_to_send = "------------------------------------------\n"
        msg_to_send += "TOUR {}\n".format(self.game.rounds_played)
        msg_to_send += "Le joueur actif est :{0}\n".format(
            self.game.current_player)
        msg_to_send += "CHOIX DU THEME :\n"
        await channel.send(msg_to_send)
        await self.send_list_of_theme_cards(channel, with_emoji=True)

    async def send_list_of_theme_cards(self, channel, with_emoji=False):
        """
        Sends the list of all themes cards identificators

        Parameters
        ----------
        channel : Channel
        with_emoji : bool
            if true, will send a prompt with emojis to click on
        """
        if with_emoji:
            self.theme_card_message_to_ident.clear()  # Clean the dictionnary
            await channel.send("Voici la liste des thèmes :")
            for theme_card_ident in self.game.get_all_theme_cards_ident(): #? Lent 
                message_sent = await channel.send("Theme {0}\n".format(theme_card_ident))
                await message_sent.add_reaction(CHECK_EMOJI)
                await message_sent.add_reaction(QUESTION_EMOJI)
                self.theme_card_message_to_ident[message_sent.id] = theme_card_ident
                logging.debug("theme_card_message_to_ident[{}] = {}".format(message_sent.id, theme_card_ident))
            await channel.send("Pour sélectionner un thème, appuyez sur ✅, pour avoir plus d'informations sur ce thème, appuyez sur ❔")
        else:
            res = "Voici la liste des thèmes : \n"
            for themeCardIdent in self.game.get_all_theme_cards_ident():
                res += "Theme " + themeCardIdent + "\n"
            await channel.send(res)

    async def send_specific_theme(self, channel, ident, with_emoji=False):
        """
        Sends the list of themes in the theme card whose `ident` is specified.

        After sending the list of themes, send the prompt to choose a theme again if the state allows it

        Parameters
        ----------
        channel : Channel
        ident : str
            the identifier of the theme card
        with_emoji : bool
            if true, will send the prompt WITH emoji
        """
        try:
            await channel.send(str(self.game.get_theme_card_by_ident(ident)))
        except ValueError:
            logging.warning("The theme '{}' was not found".format(ident))
            await channel.send("Le thème '{0}' n'existe pas :c".format(ident))
        else:
            # FIXME Check gamestate choosing theme card !
            # If everything went alright, send the prompt again
            await self.send_list_of_theme_cards(channel, with_emoji=with_emoji)

    async def choose_theme_card(self, channel, ident):
        """
        Choose a theme card corresponding to the ident in argument
        And roll for themes

        Sets the chosen_card attribute of game

        Parameters
        ----------
        channel : Channel
        ident : str
            the identifier of the theme card
        """
        try:
            self.game.chosen_card = self.game.get_theme_card_by_ident(
                ident)  # Find the theme in the theme list
        except ValueError:
            logging.warning("The theme '{}' was not found".format(ident))
            await channel.send("Le thème '{0}' n'existe pas :c".format(ident))
        else:
            # Clear the dictionnary as it will not be used anymore for this round
            self.theme_card_message_to_ident.clear()
            await self.roll_for_themes(channel)

    async def roll_for_themes(self, channel):
        """
        Rolls for two themes and present them to the player

        Sets two_available_themes attribute of game
        Sets the state to CHOOSING_THEME
        """
        self.game.state = GameState.CHOOSING_THEME
        logging.info("Changed the state to CHOOSING_THEME")
        assert self.game.chosen_card != None, "There was no chosen card"
        # Roll dices to select two themes to present to the player
        (d1, d2) = self.game.roll_two_dice()
        (theme1, theme2) = self.game.chosen_card.get_themes(d1, d2)
        logging.info("Rolled {} and {}, the associated themes are \"{}\" and \"{}\"".format(d1,d2,theme1,theme2))
        self.game.two_available_themes = (theme1, theme2)
        await channel.send("Vous avez fait un {0} et un {1}, vous avez le choix entre ces deux thèmes: ".format(d1, d2))
        msg1 = await channel.send("0 : {0}".format(theme1))
        msg2 = await channel.send("1 : {0}".format(theme2))
        self.rolled_themes = {
            msg1.id: "0",
            msg2.id: "1"
        }  # TODO : Implement roll_for_themes, and choose_theme differently at the cost of backward compatibility (change the argument value)
        logging.debug("The two rolled_theme msg id are {} and {}".format(msg1.id, msg2.id))
        await msg1.add_reaction(CHECK_EMOJI)
        await msg2.add_reaction(CHECK_EMOJI)  # TODO Factoriser
        await channel.send("Pour choisir un thème, appuyez sur {} en dessous du thème de votre choix".format(CHECK_EMOJI))

    async def choose_theme(self, channel, argument):
        """
        Selects a theme according to the argument provided.

        The argument can be either 0 or 1

        Sets game.chosen_theme
        Sets the state to PLAYING_ROUND
        """
        assert(len(self.game.two_available_themes) == 2)
        if argument not in {"0", "1"}:
            logging.warning("Argument different from 0 and 1 : {}".format(argument))
            await channel.send("The argument must be either 0 or 1")
        else:
            self.game.chosen_theme = list(self.game.two_available_themes)[
                int(argument)]  # Select theme
            await channel.send("Vous avez choisi ce thème : {}\n Maintenant, commencez à jouer !".format(self.game.chosen_theme))
            logging.info("The chosen theme was : \"{}\"".format(self.game.chosen_theme))
            self.game.state = GameState.PLAYING_ROUND
            logging.info("Changed state to PLAYING_ROUND")
            await self.prompt_to_roll(channel)

    async def prompt_to_roll(self, channel):
        """
        Displays on the channel a prompt with emojis indicating what the users can roll.

        If they click on the emojis, they roll the represented die
        """
        msg_to_send = "Il vous reste à lancer ces dés : "
        emoji_list = list(map(lambda die: die.color.to_emoji(
        ), self.game.dice[(self.game.current_colored_die_index):]))
        assert emoji_list != [], "There was no more dice to roll !"
        msg_to_send += " ".join(emoji_list) + "\n"
        if not self.game.is_white_rolled:
            # TODO : Add warning if last colored die
            msg_to_send += "Vous pouvez encore lancer le dé blanc {0} !\n".format(
                Color.WHITE.to_emoji())
        if self.game.is_any_black_die_left():
            msg_to_send += "Les autres joueurs peuvent lancer encore {0} dés noirs {1} !\n".format(
                self.game.nb_black_rolls_left, Color.BLACK.to_emoji())

        msg = await channel.send(msg_to_send)
        self.prompt_to_roll_msg_id = msg.id
        logging.debug("prompt_to_roll_msg_id is ".format(msg.id))
        # map(lambda die_emoji: await msg.add_reaction(die_emoji), emoji_list) # Nope, only the first color is useful
        await msg.add_reaction(emoji_list[0])
        if not self.game.is_white_rolled:
            await msg.add_reaction(Color.WHITE.to_emoji())
        if self.game.is_any_black_die_left:
            await msg.add_reaction(Color.BLACK.to_emoji())

    async def roll_colored_die(self, channel):
        """
        Rolls the next colored die, if available
        """
        # Check there are still colored dice to roll
        if self.game.is_any_colored_die_left():
            color = self.game.get_current_colored_die().color
            # Roll the next colored die and output the result
            prompt = self.game.roll_next_colored_die()
            logging.info("Rolled the {} die".format(color))
            await channel.send("Le dé coloré a été lancé :\n\"{}\"".format(prompt)) # TODO : Show color of die
            # Check if this was the last colored roll and end the round
            if not self.game.is_any_colored_die_left():
                await self.begin_voting(channel)
            else:
                await self.prompt_to_roll(channel)
        else:
            logging.warning("Tried to roll a colored die, when there was none to roll")
            await channel.send("Il n'y a plus de dés colorés à lancer !")

    # TODO : FACTORISE DICE ROLLING

    async def roll_white_die(self, channel):
        """
        Rolls the white die, if not already rolled
        """
        if not self.game.is_white_rolled:
            prompt = self.game.roll_white_die()
            logging.info("Rolled the white die")
            await channel.send("Le dé blanc a été lancé :\n\"{}\"".format(prompt)) # TODO : Show color of die
            await self.prompt_to_roll(channel)
        else:
            logging.warning("Tried to roll a white die, when there was none to roll")
            await channel.send("Le dé blanc a déjà été lancé !")

    async def roll_black_die(self, channel):
        """
        Rolls the black die, if available
        """
        if self.game.is_any_black_die_left():
            prompt = self.game.roll_black_die()
            logging.info("Rolled the black die")
            await channel.send("Le dé noir a été lancé :\n\"{}\"".format(prompt)) # TODO : Show color of die
            await self.prompt_to_roll(channel)
        else:
            logging.warning("Tried to roll a black die, when there was none to roll")
            await channel.send("Il n'y a plus de dés noirs disponibles..")

    async def begin_voting(self, channel):
        """
        Ends the playing round, and begins a vote about the current player

        Changes the state of the game to GameState.VOTING
        """
        assert not self.game.is_any_colored_die_left(), "There were still colored dice to roll."
        self.game.state = GameState.VOTING
        logging.info("Changed the state to VOTING")
        await channel.send("Il n'y a plus de dés colorés, c'est la fin du tout !\nC'est l'heure de mesurer le succès de {} !".format(self.game.current_player.user.name))
        # Stores all the players who are *not* the current_player
        self.users_left_to_vote = list(map(lambda player: player.user,list(filter(lambda player : player != self.game.current_player, self.game.players))))
        if self.users_left_to_vote != []:
            await self.vote_prompt(channel)
        else:
            logging.error("There was no player other than the current player. No vote.")

    async def vote_prompt(self, channel):
        """
        Makes the current player roll a die, and adds it to its score. Then, asks the other players if they think he deserves more or not.

        Adds theses reactions to the message sent : THUMBS_UP_EMOJI, THUMBS_DOWN_EMOJI. They allow the users to vote
        """
        d1 = self.game.roll_die() # TODO : Factorise with the second die roll
        self.game.add_score_to_current_player(d1)
        logging.info("Current player rolled a {}, current score is {}".format(d1, self.game.current_player.score))
        msg_to_send = "{0} a lancé un dé et a obtenu {1}.\nPensez vous qu'il mérite plus ?\n".format(self.game.current_player.user.name, d1) # TODO : Emphasise on the score
        # TODO Refactor above line in begin_voting
        msg_to_send += "Donnez votre avis avec {0} ou {1}".format(THUMBS_UP_EMOJI, THUMBS_DOWN_EMOJI)
        msg = await channel.send(msg_to_send)
        self.prompt_to_vote_msg_id = msg.id
        logging.debug("prompt_to_vote_msg_id is {}".format(msg.id))
        await msg.add_reaction(THUMBS_UP_EMOJI)
        await msg.add_reaction(THUMBS_DOWN_EMOJI)

    async def vote(self, channel, user, vote_value):
        """
        Registers a vote made by a user, and if there are no more users left to vote, ends the round.

        When all the users have voted, tots up all the votes, and if the sum is positive or zero,
        the current player can roll a second die.
        Otherwise, he doesn't.

        When the votes are done, switch to the next player and begins another round.


        Parameters
        ----------
        channel : Channel
            The channel where the vote happened
        user : User
            The user who voted
        vote_value : int
            The value of the vote (e.g. -1, 0, or 1)
        
        Warns
        -----

        Notes
        -----
        Indirectly changes the state of the game via self.prepare_round()
        """
        self.users_left_to_vote.remove(user) # FIXME Handle Error case
        self.votes.append(vote_value)
        logging.debug("Adding this vote : {}, by user {}".format(vote_value, user.name))
        if self.users_left_to_vote == []:
            logging.info("End of vote. Doing the total...")
            # Check if total of the votes are positive or not
            total = sum(self.votes)
            logging.debug("Total of the votes : {}".format(total))
            msg_to_send = ""
            if total >= 0:
                d1 = self.game.roll_die()
                self.game.add_score_to_current_player(d1)
                logging.info("Player rolled a second die, obtaining {}, current score is {}".format(d1, self.game.current_player.score))
                msg_to_send += "Vous êtes au moins la moitié à penser qu'il mérité plus, alors, "
                msg_to_send += "{0} a le droit de relancer un dé ! \n".format(self.game.current_player.user.name)
                msg_to_send += "{0} a obtenu {1} a son lancer de dés, son score est a présent de {2}.\n".format(self.game.current_player.user.name, d1, self.game.current_player.score)
                msg_to_send += "Félicitations !"
            else:
                logging.info("Player did not roll a second die")
                msg_to_send += "Vous êtes au moins la moitié à penser qu'il ne mérite pas plus, alors, "
                msg_to_send += "{0} ne relance pas son dé, et son score est a présent de {1}.\n".format(self.game.current_player.user.name, self.game.current_player.score)
                msg_to_send += "Bravo quand même !"
            await channel.send(msg_to_send)
            self.game.switch_to_next_player() #? Delegate this to another function ? Wrap it ?
            await self.prepare_round(channel)

    async def vote_up(self, channel, user):
        """
        Called when an user votes positively.
        Makes a positive vote using self.vote()
        """
        await self.vote(channel, user, 1)

    async def vote_down(self, channel, user):
        """
        Called when a user votes negatively.
        Makes a negative vote using self.vote()
        """
        await self.vote(channel, user, -1)

    async def send_message_cannot_use_now(self, channel):
        """
        Sends a message on `channel` indicating the command used cannot be used right now
        """
        logging.warning("Tried to use a command at the wrong time")
        await channel.send("Cette commande ne peut pas être utilisée maintenant.")


if __name__ == "__main__":
    logging.basicConfig(filename="log.txt", level=logging.INFO)
    game = AdopteDragonGame()
    client = AdopteDragonBot(game)
    client.game.load_theme_cards("adopteDragonBot/resources/themes")
    client.game.load_dice("adopteDragonBot/resources/dice")
    client.run('Njk4NTcwMDYxMDYwODk4ODI2.XpHwbw.cY4EtMLWtX7dmIzqadhBG_mvDoE')
