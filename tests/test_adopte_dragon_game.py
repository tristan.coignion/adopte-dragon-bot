from context import adopteDragonBot
from adopteDragonBot.adopte_dragon_game import AdopteDragonGame
from unittest.mock import Mock
import pytest

YELLOW_RAW = """La semaine dernière
Je vous ai jamais dit
Un beau jour
Tout le monde pense
Quand j'étais petit
Je connais quelqu'un"""

ORANGE_RAW = """En plus
Mais vous savez quoi
En réalité
Et croyez-moi
Alors moi
Et puis"""


def setup():
    g = AdopteDragonGame()
    g.load_theme_cards("resources/themes")
    g.load_dice("resources/dice")
    # print(g.dice)
    # print(g.dice[0] > g.dice[1])
    # print(g.white_die.faces)
    # print(g.roll_next_colored_die())
    # print(g.roll_next_colored_die())
    # print(g.roll_next_colored_die())
    # print(g.roll_next_colored_die())
    # print(g.roll_next_colored_die())
    # print(g.is_any_colored_die_left())
    return g

# PLAYERS

def add_two_players(g):
    user1 = Mock()
    user2 = Mock()
    g.add_player(user1)
    g.add_player(user2)
    return g, user1, user2

def test_add_player_adds_the_user():
    g = setup()
    user1 = Mock()
    user2 = Mock()
    assert(len(g.players) == 0)
    g.add_player(user1)
    assert(len(g.players) == 1)
    g.add_player(user2)
    assert(len(g.players) == 2)
    assert(g.players[0].user == user1)
    assert(g.players[1].user == user2)


def test_cannot_add_twice_same_user():
    g = setup()
    mockUser = Mock()
    mockUser.id = 5
    g.add_player(mockUser)

    with pytest.raises(ValueError):
        g.add_player(mockUser)
    assert(len(g.players) == 1)

def test_is_user_playing_returns_true_if_user_is_a_player():
    g = setup()
    user1 = Mock()
    user2 = Mock()
    g.add_player(user1)
    assert(g.is_user_playing(user1))
    assert(not g.is_user_playing(user2))

def test_remove_player_removes_the_player():
    g = setup()
    g, user1, user2 = add_two_players(g)
    g.remove_player(user1)
    assert(len(g.players) == 1)
    assert(g.players[0].user == user2)
    g.remove_player(user2)
    assert(len(g.players) == 0)

def test_cannot_remove_unexisting_player():
    g = setup()
    user1 = Mock()
    user2 = Mock()
    g.add_player(user1)
    with pytest.raises(ValueError):
        g.remove_player(user2)

def test_set_first_player_sets_current_player():
    g = setup()
    g, user1, _ = add_two_players(g)
    g.set_first_player()
    assert(g.current_player.user == user1)

def test_set_first_player_crashs_when_no_players():
    g = setup()
    with pytest.raises(RuntimeError):
        g.set_first_player()

def test_add_score_adds_score():
    g = setup()
    g, _, _ = add_two_players(g)
    g.set_first_player()
    g.add_score_to_current_player(50)
    assert(g.players[0].score == 50)

def test_add_score_crashs_when_no_current_player():
    g = setup()
    user1 = Mock()
    g.add_player(user1)
    with pytest.raises(RuntimeError):
        g.add_score_to_current_player(50)

def test_switch_to_next_player_switchs_the_current_player():
    g = setup()
    g, user1, user2 = add_two_players(g)
    g.set_first_player()
    g.switch_to_next_player()
    assert(g.current_player.user == user2)
    assert(g.current_player_index == 1)

    g.switch_to_next_player()
    assert(g.current_player.user == user1)
    assert(g.current_player_index == 0)

def test_switch_to_next_player_crashs_when_no_player():
    g = setup()
    with pytest.raises(RuntimeError) as excinfo:
        g.switch_to_next_player()
    assert("no player" in str(excinfo.value))

def test_switch_to_next_player_crashs_when_no_current_player():
    g = setup()
    g, _, _ = add_two_players(g)
    with pytest.raises(RuntimeError) as e:
        g.switch_to_next_player()
    assert("no current player" in str(e.value))

# Themes Cards

def test_load_theme_cards_works():
    g = setup()
    assert(len(g.theme_cards) == 2)
    t1 = g.theme_cards[0]
    t2 = g.theme_cards[1]
    assert(t1.ident == "A")
    assert(t2.ident == "B")
    assert(t1.themes[0][1] == "Theme 12")

def test_get_theme_ident_returns_all_idents():
    g = setup()
    all_idents = g.get_all_theme_cards_ident()
    assert("A" in all_idents)
    assert("B" in all_idents)
    assert(len(all_idents) == 2)

def test_get_theme_card_by_ident_returns_correct_card():
    g = setup()
    assert(g.get_theme_card_by_ident("A") == g.theme_cards[0])
    assert(g.get_theme_card_by_ident("B") == g.theme_cards[1])

def test_get_theme_card_by_ident_crashs_when_no_correponding_ident():
    g = setup()
    with pytest.raises(ValueError):
        g.get_theme_card_by_ident("C")

# Dice

def test_load_dice_works():
    g = setup()
    assert(len(g.dice) == 5)
    assert(g.black_die != None)
    assert(g.white_die != None)
    assert(g.dice[0].faces[0] == "La semaine dernière")
    assert(g.dice[4].faces[5] == "Et tenez-vous bien")
    assert(g.black_die.faces[0] == "Ah bon ? Pourquoi ?")
    assert(g.white_die.faces[0] == "Et là GRRRR")

def test_dice_in_correct_order():
    g = setup()
    assert(sorted(g.dice) == g.dice)

def test_roll_die_return_integer_between_1_and_6():
    g = setup()
    for _ in range(0,100):
        assert( 1 <= g.roll_die() <= 6)

def test_roll_two_dice_return_two_integers_between_1_and_6():
    g = setup()
    for _ in range(0,100):
        assert( (1,1) <= g.roll_two_dice() <= (6,6))

def test_reset_dice_resets_the_values():
    g = setup()
    g.is_white_rolled = True
    g.nb_black_rolls_left = 1
    g.current_colored_die_index = 5
    g.reset_dice()
    assert(not g.is_white_rolled)
    assert(g.nb_black_rolls_left == 3)
    assert(g.current_colored_die_index == 0)

def test_roll_next_rolls_next_die():
    g = setup()
    d1 = g.roll_next_colored_die()
    assert(d1 in YELLOW_RAW)
    d2 = g.roll_next_colored_die()
    assert(d2 in ORANGE_RAW)

def test_roll_next_crashs_when_no_more_dice():
    g = setup()
    for _ in range(0,5):
        g.roll_next_colored_die()
    with pytest.raises(RuntimeError) as err:
        g.roll_next_colored_die()
    assert("no more dice" in str(err.value))

def test_is_any_colored_die_left():
    g = setup()
    for _ in range(0,5):
        assert(g.is_any_colored_die_left())
        g.roll_next_colored_die()
    assert(not g.is_any_colored_die_left())

def test_roll_white_returns_string():
    g = setup()
    prompt = g.roll_white_die()
    assert(type(prompt) == type(""))
    assert(len(prompt) > 0)

def test_roll_white_die_sets_is_white_rolled_to_true():
    g = setup()
    assert(not g.is_white_rolled)
    g.roll_white_die()
    assert(g.is_white_rolled)

def test_roll_white_die_crashs_when_already_rolled():
    g = setup()
    g.roll_white_die()
    with pytest.raises(RuntimeError) as err:
        g.roll_white_die()
    assert("has already been rolled" in str(err.value))

def test_roll_black_returns_string():
    g = setup()
    prompt = g.roll_black_die()
    assert(type(prompt) == type(""))
    assert(len(prompt) > 0)

def test_roll_black_die_decrements_nb_rolls_left():
    g = setup()
    assert(g.nb_black_rolls_left == 3)
    g.roll_black_die()
    assert(g.nb_black_rolls_left == 2)

def test_roll_black_die_crashs_when_no_rolls_left():
    g = setup()
    for _ in range(0,3):
        g.roll_black_die()
    with pytest.raises(RuntimeError) as err:
        g.roll_black_die()
    assert("no more black rolls" in str(err.value))

def test_is_any_black_die_left():
    g = setup()
    for _ in range(0,3):
        assert(g.is_any_black_die_left())
        g.roll_black_die()
    assert(not g.is_any_black_die_left())