class Player:
  """
  A player is defined by a discord user and a score

  Attributes
  ----------
    user: user
      the discord user representing the player
    score : int
      the score of the player
  """
  def __init__(self, user):
    self.user = user
    self.score = 0

  def __eq__(self, other):
    """Two players are equal iff the id of their users are equals"""
    return isinstance(other, Player) and self.user.id == other.user.id

  def __str__(self):
    """
    '`user.name` : `score`'
    """
    return self.user.name + " : " + str(self.score)